public class AccountAppointmentsController {
	public List<calEvent> events {get;set;}
    public Account acct;
	String dtFormat = 'EEE, d MMM yyyy HH:mm:ss z';
    
    public AccountAppointmentsController(ApexPages.StandardController  stdController) {
        this.acct = (Account)stdController.getRecord();
        List<Contact> acctContacts=[SELECT Id,Name from Contact where Account.Id=:acct.Id];
        events=new List<calEvent>();
        
        //set of contact ids to just make a soql query for the appointments
        Set<Id> contactIdSet=new Set<Id>();
        Map<Id,Contact> contactsMap=new Map<Id,Contact>();
        for(Contact c:acctContacts){
            contactIdSet.add(c.Id);
            contactsMap.put(c.Id,c);
        }
        
        List<Contact_Appointment__c> appointmentsList=[select Id,Title__c,StartDate__c,EndDate__c,Is_All_Day__c,Invitation_Status__c , 
                                                       Location__c, Appointment_Contact__c,Appointment_Contact__r.Name                                                        
                                 from Contact_Appointment__c where Appointment_Contact__r.Id in :contactIdSet];
        
        for(Contact_Appointment__c appointment:appointmentsList){
            DateTime startDT = appointment.StartDate__c;
            DateTime endDT = appointment.EndDate__c;
            calEvent tempEvent = new calEvent();
             
            tempEvent.title = appointment.Appointment_Contact__r.Name + ' ' + appointment.Title__c;
            tempEvent.allDay = appointment.Is_All_Day__c;
            tempEvent.startString = startDT.format(dtFormat);
            tempEvent.endString = endDT.format(dtFormat);
            tempEvent.url = '/' + appointment.Id;
            switch on appointment.Invitation_Status__c {
                when 'Pending Confirmation' {
                    tempEvent.className = 'confirmation-pending';
                }    
                when 'Accepted' {
                    tempEvent.className = 'confirmation-accepted';
                }
                when 'Rejected' {
                    tempEvent.className = 'confirmation-rejected';
                }
                when else {
                    tempEvent.className = 'confirmation-pending';
                }
            }
            
            events.add(tempEvent);
        }
        
    }
    //wrapper class for fullcalendar object list
    public class calEvent{
        public String title {get;set;}
        public Boolean allDay {get;set;}
        public String startString {get;set;}
        public String endString {get;set;}
        public String url {get;set;}
        public String className {get;set;}
    }
}