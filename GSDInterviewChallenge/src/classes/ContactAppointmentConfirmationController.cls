public class ContactAppointmentConfirmationController {
    public Contact_Appointment__c appointment;
    public ContactAppointmentConfirmationController(ApexPages.StandardController  stdController) {
        this.appointment = (Contact_Appointment__c)stdController.getRecord();
    }
    
    public PageReference Approve(){
        appointment.Invitation_Status__c='Accepted';
        update appointment;
        return new PageReference('/apex/ContactAppointmentConfirmationThanks');
    }
    
    public PageReference Reject(){
        appointment.Invitation_Status__c='Rejected';
        update appointment;
        return new PageReference('/apex/ContactAppointmentConfirmationThanks');
    }
}