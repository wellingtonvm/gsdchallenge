@isTest
public class ContactAppointmentConfirmationTest {
    @isTest static void AcceptOrRejectInvite(){
        Account newAccount=new Account(Name='Account Appointment Calendar Test',Phone='888-888-8888');
        insert newAccount;
        //Inserting the contact that will receive three invite to test the different event status
        Contact testContact=new Contact(FirstName='GSD',LastName='Company',Email='test@gsdcompany.com',AccountId=newAccount.Id);
        insert testContact;
        
        //insert the invite to accept
        Contact_Appointment__c pendingAppointment=new Contact_Appointment__c(Title__c='First test event',
                                                                             Description__c='Unit testing purpose',
                                                                            Location__c='GSD Company',
                                                                            StartDate__c=Datetime.now().addDays(2),
                                                                            EndDate__c=Datetime.now().addDays(2).addHours(2),
                                                                            Appointment_Contact__c=testContact.Id,
                                                                            Invitation_Status__c='Pending Confirmation');
        
        insert pendingAppointment;
        
        ApexPages.StandardController myStdController = new ApexPages.StandardController(pendingAppointment);
        ContactAppointmentConfirmationController confirmController = new ContactAppointmentConfirmationController(myStdController);
        
        PageReference pageRef = Page.ContactAppointmentConfirmation;
        pageRef.getParameters().put('id', pendingAppointment.Id);
        Test.setCurrentPage(pageRef);
        //accept invite
        confirmController.Approve();
        //verify if was accepted
        Contact_Appointment__c acceptedAppointment=[Select Id,Invitation_Status__c from Contact_Appointment__c where Id=:pendingAppointment.Id limit 1];
        System.assert(acceptedAppointment.Invitation_Status__c=='Accepted');
        //reject invite
        confirmController.Reject();
        //verify if was accepted
        Contact_Appointment__c rejectedAppointment=[Select Id,Invitation_Status__c from Contact_Appointment__c where Id=:pendingAppointment.Id limit 1];
        System.assert(rejectedAppointment.Invitation_Status__c=='Rejected');
    }

    
    
}