@isTest
public class AccountAppointmentsControllerTest {
	@isTest static void CalendarExtensionControllerTest(){
        //Creating the test account
        Account newAccount=new Account(Name='Account Appointment Calendar Test',Phone='888-888-8888');
        insert newAccount;
        //Inserting the contact that will receive three invite to test the different event status
        Contact testContact=new Contact(FirstName='GSD',LastName='Company',Email='test@gsdcompany.com',AccountId=newAccount.Id);
        insert testContact;
        //Creating the events
        List<Contact_Appointment__c> appointmentsList=new List<Contact_Appointment__c>();
        
        Contact_Appointment__c pendingAppointment=new Contact_Appointment__c(Title__c='First test event',
                                                                             Description__c='Unit testing purpose',
                                                                            Location__c='GSD Company',
                                                                            StartDate__c=Datetime.now().addDays(2),
                                                                            EndDate__c=Datetime.now().addDays(2).addHours(2),
                                                                            Appointment_Contact__c=testContact.Id,
                                                                            Invitation_Status__c='Pending Confirmation');
        appointmentsList.add(pendingAppointment);
        
        Contact_Appointment__c confirmedAppointment=new Contact_Appointment__c(Title__c='Second test event',
                                                                             Description__c='Unit testing purpose',
                                                                            Location__c='GSD Company',
                                                                            StartDate__c=Datetime.now().addDays(3),
                                                                            EndDate__c=Datetime.now().addDays(3).addHours(2),
                                                                            Appointment_Contact__c=testContact.Id,
                                                                            Invitation_Status__c='Accepted');
        appointmentsList.add(confirmedAppointment);
        
        Contact_Appointment__c rejectedAppointment=new Contact_Appointment__c(Title__c='Third test event',
                                                                             Description__c='Unit testing purpose',
                                                                            Location__c='GSD Company',
                                                                            StartDate__c=Datetime.now().addDays(4),
                                                                            EndDate__c=Datetime.now().addDays(4).addHours(2),
                                                                            Appointment_Contact__c=testContact.Id,
                                                                            Invitation_Status__c='Rejected');
        appointmentsList.add(rejectedAppointment);
        //inserting the events list
        insert appointmentsList;
        //testing the calendar extension controller
        ApexPages.StandardController myStdController = new ApexPages.StandardController(newAccount);
        AccountAppointmentsController accCalendar = new AccountAppointmentsController(myStdController);
        
        PageReference pageRef = Page.AccountAppointmentsCalendar;
        pageRef.getParameters().put('id', newAccount.Id);
        Test.setCurrentPage(pageRef);
    }
}